Framework '4.5.1'

properties {
  $project_name = 'Iteration.Zero'

  $version = "1.0.0.1"
  $configuration = 'Debug'

  $base_dir = resolve-path .
  $build_dir = "$base_dir\build"
  $temp_package_dir = "$base_dir\temp_package"
  $source_dir = "$base_dir\src"
  $test_dir = "$base_dir\UnitTest"
  $result_dir = "$base_dir\result"
  $packages_dir = "$source_dir\packages"
  $app_bin_dir = "$source_dir\Wellbore.Answer.UI\bin"
  $nuget_dir = "$base_dir\NuGet"
}

FormatTaskName "==== Running Task: {0}"

task default -depends SetDebugBuild, Clean, Test, Info

task Info {
  Write-Host "*************************************************************************************"
  Write-Host " "
  Write-Host "        Release Number: $version"
  Write-Host "         NuGet Package: $nuget_dir"
  Write-Host " "
  Write-Host "*************************************************************************************"
}

task help {
  Write-Help-Header
  Write-Help-Section-Header "Comprehensive Building"
  Write-Help-For-Alias "(default)" "Intended for first build or when you want a fresh, clean local copy"

  Write-Help-Section-Header "Running Tests"
  Write-Help-Footer
  exit 0
}

task SetDebugBuild {
  $script:configuration = 'Debug'
}

task SetReleaseBuild {
  $script:configuration = 'Release'
}

task Test -depends Compile {
	create_directory($result_dir)
	$nunitRunner = join-path $source_dir "packages\NUnit.Runners.2.6.3\tools\nunit-console-x86.exe"
	& $nunitRunner /xml:$result_dir\console-test.xml $source_dir\$project_name.Test\bin\$script:configuration\$project_name.Test.dll /nologo /nodots /framework:net-4.6

	if ($lastexitcode -gt 0)
	{
		throw "{0} unit tests failed." -f $lastexitcode
	}
	if ($lastexitcode -lt 0)
	{
		throw "Unit test run was terminated by a fatal error."
	}
}

task Compile -depends CommonAssemblyInfo {
  $script:remote_deploy = 0
  exec { msbuild /t:build /v:q /nologo /m /p:Configuration=$script:configuration $source_dir\$project_name.sln }
}

task Clean  {
  delete_file $package_file
  delete_directory $build_dir
  create_directory $build_dir
  create_directory $test_dir 
  create_directory $result_dir
  exec { msbuild /t:clean /v:q /p:Configuration=$script:configuration $source_dir\$project_name.sln }
}

task CommonAssemblyInfo {
	$date = Get-Date
	$year = $date.Year

"using System.Reflection;
using System.Runtime.InteropServices;

[assembly: ComVisible(false)]
[assembly: AssemblyCompany(""DEV Name"")]
[assembly: AssemblyProduct(""Iteration"")]
[assembly: AssemblyCopyright(""Copyright Info 2016\nAll rights reserved."")]
[assembly: AssemblyVersion(""$version"")]
[assembly: AssemblyConfiguration(""$script:configuration"")]" | out-file "$source_dir\CommonAssemblyInfo.cs" -encoding "UTF8"
}

# Utility Methods 
function Write-Help-Header($description) {
  Write-Host ""
  Write-Host "********************************" -foregroundcolor DarkGreen -nonewline;
  Write-Host " HELP " -foregroundcolor Green  -nonewline; 
  Write-Host "********************************"  -foregroundcolor DarkGreen
  Write-Host ""
  Write-Host "This build script has the following common build " -nonewline;
  Write-Host "task " -foregroundcolor Green -nonewline;
  Write-Host "aliases set up:"
}


function global:delete_file($file) {
	if($file) { remove-item $file -force -ErrorAction SilentlyContinue | out-null } 
}

function global:delete_directory($directory_name) {
  rd $directory_name -recurse -force  -ErrorAction SilentlyContinue | out-null
}

function global:create_directory($directory_name) {
  mkdir $directory_name  -ErrorAction SilentlyContinue  | out-null
}

function global:copy_directory($source_dir, $dest_dir) {
  xcopy $source_dir $dest_dir | out-null
}